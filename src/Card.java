/**
 * NAME:		 Joseph Dappa
 */


//Class to encapsulate a Card objects
public class Card 
{
	//Card rank and suit
	int rank = 0, suit = 0;
	
	//Carde contructor
	public Card()
	{
		
	}
	//Another constructor
	public Card(int r, int s)
	{
		rank = r;
		suit = s;
	}
	
	private int getRank() 
	{
		return rank;
	}
	
	private int getSuit() 
	{
		return suit;
	}
	
	public boolean rankIsLessThan(Card c)
	{
		return(rank < c.getRank());
	}//rankIsLessThan()
	
	public boolean rankIsGreaterThan(Card c)
	{
		return(rank > c.getRank());
	}//rankIsGreaterThan
	
	public boolean rankIsEqualTo(Card c)
	{
		return(rank == c.getRank());
	}//rankIsGreaterThan
	
	public String toString()
	{
		//Strings to store rank, suit, and card representation
		//These will be assigned later in this method
		String cardSuit = "";
		String cardRank = "";
		String cardString = "";
		
		//Get values for card suit and rank
		int cs = getSuit();
		int cr = getRank();
		
		//Switch block to compare suit (H,D,C,S)
		
		switch(cs)
		{
			case 0:
				cardSuit = "hearts";
				break;
				
			case 1:
				cardSuit = "diamonds";
				break;
			case 2:
				cardSuit = "clubs";
				break;
			case 3:
				cardSuit = "spades";
				break;
				
				default:
					cardSuit = "n/a";	
		}//Switch
		
		//Got suit if the card is valid. Now get rank
		
		switch(cr)
		{
			case 1:
				cardRank = "ace";
				break;
			case 2:
				cardRank = "2";
				break;
			case 3:
				cardRank = "3";
				break;
			case 4:
				cardRank = "4";
				break;
			case 5:
				cardRank = "5";
				break;
			case 6:
				cardRank = "6";
				break;
			case 7:
				cardRank = "7";
				break;
			case 8:
				cardRank = "8";
				break;
			case 9:
				cardRank = "9";
				break;
			case 10:
				cardRank = "10";
				break;
			case 11:
				cardRank = "jack";
				break;
			case 12:
				cardRank = "queen";
				break;
			case 13:
				cardRank = "king";
				break;
				default:
					cardRank = "n/a";	
		}//end of switch rank
		
		//Now concatenate a string representation of the card
		cardString = cardRank + "_of_" + cardSuit + ".png";
		
		return cardString;
	}//end toString()
	

}//end of card
