/**
 * NAME:		 Joseph Dappa
 */



import java.util.Random;


//Class to encapsulate a deck of card objects (52, no jokers).
public class DeckOfCards 
{
	//Variables initialisation
	private int oldCard,newCard;
	private int[] suits = new int[4];
	private int[] ranks = new int[13];
	
	private Card[] deck = new Card[52];
	
	public DeckOfCards()
	{
		createDeck();
		restart();
	}
	//To restart the position of the new and old cards
	public void restart()
	{
		newCard = deck.length;
		oldCard = -1;
	}
	//The deck is being populated here using a nested for loop
	public void createDeck()
	{
		int c=0;
		for(int suit=0;suit<suits.length;suit++)
		{
			for(int rank=1;rank<=ranks.length;rank++)
			{
				if (c < deck.length)
				{
					deck[c] = new Card(rank, suit);
					c++;
				}
			}
		}
	}
	
	//Method to shuffle deck
	public void shuffle()
	{
		Random rand = new Random();
		for(int card=0;card<deck.length;card++)
		{
			//Generate a random card
			int randomCard = rand.nextInt(52);

			//hold the card iterated temporarily
			Card temp = deck[card];
			//choose the randomly generated card
			deck[card] = deck[randomCard];
			//swap the position of temp card to the position of random card
			deck[randomCard] = temp;
		}
	}//end of shuffle
	
	
	public boolean isEmpty()
	{
		return(newCard<0);	
	}
	
	//Dealing top card
	/**
	 * At first, It checks if the deck is empty, will
	 * populate the deck, else a new card will be dealt.*/
	public Card dealTopCard()
	{
		if(isEmpty())
			createDeck();
		else
		{
			oldCard = newCard;
			newCard--;
		}
		return deck[newCard];
		
	}
	/**
	 * The boolean methods are used to compare the cards to check
		if the new card is greater or lesser than theold card*/
	public boolean checkGreater()
	{
		return deck[newCard].rankIsGreaterThan(deck[oldCard]);
	}
	public boolean checkLower()
	{
		return deck[newCard].rankIsLessThan(deck[oldCard]);
	}
	public boolean checkEqual()
	{
		return deck[newCard].rankIsEqualTo(deck[oldCard]);
	}
}