/**
 * NAME:		 Joseph Dappa
 */


	//Standard imports
	import javafx.application.Application;
	import javafx.application.Platform;
	import javafx.stage.Stage;
	import javafx.scene.Scene;

	//Imports of other components
	import javafx.scene.control.Label;
	import javafx.scene.control.Menu;
	import javafx.scene.control.Button;
	import javafx.scene.control.MenuBar;
	import javafx.scene.control.MenuItem;
	import javafx.scene.control.ProgressBar;
	import javafx.scene.control.ProgressIndicator;
	import javafx.scene.control.RadioButton;
	import javafx.scene.control.ToggleGroup;
	import javafx.scene.image.ImageView;
	import javafx.scene.image.Image;
	import javafx.scene.paint.Color;
	import javafx.geometry.Insets;
	import javafx.geometry.Pos;

	//Imports layout
	import javafx.scene.layout.BorderPane;
	import javafx.scene.layout.GridPane;
	import javafx.scene.layout.HBox;
	import javafx.scene.layout.VBox;
	
	//Imports to create animation and time.
	import javafx.util.Duration;
	import javafx.animation.KeyFrame;
	import javafx.animation.KeyValue;
	import javafx.animation.Timeline;

	//Import file 
	import java.io.File;


	public class HiLoGame extends Application
	{
		//Declaration of variables
		private Label lblTopMessage,lblFirstCard,lblNext,lblSecondCard,lblResult,lblWinScore;
		private Button btnFirstDeal,btnSecondDeal;
		private RadioButton rbHigher, rbLower;
		private MenuBar mBar;
		private Menu menuFile,menuHelp;
		private MenuItem newGame, shuffleDeck, exitGame,about;
		private ToggleGroup tg;
		private Image img;
		private ImageView imgViewFirstCard, imgViewSecondCard;
		private ProgressIndicator progInd;
		private ProgressBar progBar;
		DeckOfCards deck;
		private int shuffled=0,gameplayed=0,consecutiveWins=5,numOfWins=0,btnCheck=0,maxGame=26;
		private boolean endGame=false;

		public HiLoGame()
		{}
		
		/**The Initialisation Method init()*/
		@Override
		public void init()
		{
			/**Instantiate components*/
			//Label
			lblTopMessage = new Label("New Game");
			lblFirstCard = new Label("First Card Dealt:");
			lblNext = new Label("Next card will be:");
			lblSecondCard = new Label("Second Card Dealt:");
			lblResult = new Label("::");
			lblWinScore = new Label("Score: 0/5");
			
			//Button
			btnFirstDeal = new Button("<-Deal First Card");
			btnSecondDeal = new Button("Deal Second Card->");
			//Button size
			btnFirstDeal.setMaxWidth(180);
			btnSecondDeal.setMaxWidth(180);
			
			//RadioButton
			rbHigher = new RadioButton("Higher"); 
			rbLower = new RadioButton("Lower");
			
			//Progress bar and indicator
			progBar = new ProgressBar(0);
			progInd = new ProgressIndicator(0);
			
			//config the min width and min height
			progBar.setMinSize(300, 20);
			
			//ToggleGroup
			tg = new ToggleGroup();
			
			//Set RadioButton to togglegroup
			rbHigher.setToggleGroup(tg);
			rbLower.setToggleGroup(tg);
			
			//Create deck of cards
			deck = new DeckOfCards();
			
			
			//ImageView
			imgViewFirstCard = new ImageView();
			imgViewSecondCard = new ImageView();
			
			//MenuBar
			mBar = new MenuBar();
			//Menu
			menuFile = new Menu("File");
			menuHelp = new Menu("Help");
			
			//MenuItem
			newGame = new MenuItem("New Game");
			shuffleDeck =  new MenuItem("Shuffle");
			exitGame = new MenuItem("Exit");
			about = new MenuItem("About");
			
			//Append each MenuItem to Menu
			menuFile.getItems().add(newGame);
			menuFile.getItems().add(shuffleDeck);
			menuFile.getItems().add(exitGame);
			menuHelp.getItems().add(about);
			
			//Append Menu to MenuBar
			mBar.getMenus().add(menuFile);
			mBar.getMenus().add(menuHelp);
			
			/**Events Handlers*/
			
			//New game handler
			newGame.setOnAction(ae->newGame());
			
			//Shuffle card
			/**To shuffle, I checked if the game is running*/
			shuffleDeck.setOnAction(ae->
			{
				//Increment to one when shuffle is executed
				++shuffled;
				//Check if game played is greater than zero that mean one can't shuffle during game
				if(gameplayed>0)
					showMessage("#9E260C","Sorry you cannot shuffle during game.","top");
				//When a game ends, you will only allowed to shuffle when you load new game.
				if(endGame==true)
					showMessage("#9E260C","You can only shuffle after loading new game.","top");
				else
				{
					//Shuffle the deck five times to make prediction difficult.
					for(int i=0;i<5;i++) {deck.shuffle();}
					showMessage("green","Deck shuffled successfully.","top");
				}
			});//End of shuffle event handler
			
			//Exit event handler
			exitGame.setOnAction(ae->Platform.exit());
			
			//About event handler
			about.setOnAction(ae->showAbout());
			
			//Deal buttons handler
			btnFirstDeal.setOnAction(ae->dealFirstCard());
			btnSecondDeal.setOnAction(ae->dealSecondCard());
			
		    //At the start of new game, the radio buttons and second deal button are disabled	
			btnDisable(true);
		}
		
		@Override
		public void start(Stage pStage) throws Exception 
		{
			//Set title
			pStage.setTitle("HiLo Card Game V1.1.0");
			//Set height and width
			pStage.setWidth(600);
			pStage.setHeight(400);
			//Set resizable to false
			pStage.setResizable(false);
			
			/**Create layouts*/
			//BorderPane
			BorderPane bp = new BorderPane();
			//GridPane
			GridPane gp = new GridPane();
			//Main VBox
			VBox vbMain = new VBox();
			//Create HBox for top message
			HBox hbTopMessage = new HBox();
			//Create ProgressBar HBox
			HBox hbProcessBarInd = new HBox();
			
			//Set Alignment of hbTopMessage and hbProcessBarInd
			hbTopMessage.setAlignment(Pos.BOTTOM_CENTER);
			hbProcessBarInd.setAlignment(Pos.BOTTOM_CENTER);
			
			//Create minute vboxes for 1st card, middle components and 2nd card
			VBox vbFirstCol = new VBox();
			VBox vbMiddleCol = new VBox();
			VBox vbLastCol = new VBox();
			
			//Create a HBox to hold above VBoxes
			HBox hbEncapsulate = new HBox();
			
			//Add spacing and padding in each minute VBoxes
			vbFirstCol.setSpacing(10);
			vbFirstCol.setPadding(new Insets(20));
			
			vbMiddleCol.setPadding(new Insets(40));
			vbMiddleCol.setSpacing(10);
			
			vbLastCol.setSpacing(10);
			vbLastCol.setPadding(new Insets(20));
			
			
			//Append lblTopMessage to hbTopMessage
			hbTopMessage.getChildren().add(lblTopMessage);
			//Append progBar, progInd and lblWinScore to hbProcessBarInd
			hbProcessBarInd.getChildren().addAll(progBar,lblWinScore,progInd);
			
			//Manage spacing
			hbProcessBarInd.setSpacing(10);
					
			//Append each component to its respective VBox
			vbFirstCol.getChildren().addAll(lblFirstCard,imgViewFirstCard);
			vbMiddleCol.getChildren().addAll(lblNext,rbHigher,rbLower,btnFirstDeal,btnSecondDeal);
			vbLastCol.getChildren().addAll(lblSecondCard,imgViewSecondCard,lblResult);
		

			//Append the minutes VBoxes to hbEncapsulate
			hbEncapsulate.getChildren().addAll(vbFirstCol,vbMiddleCol,vbLastCol);
			
			//Append hbTopMessage,hbEncapsulate and hbProcessBarInd to vbMain
			vbMain.getChildren().addAll(hbTopMessage,hbEncapsulate,hbProcessBarInd);
			
			//Set Hgap and Vgap in GridPane
			gp.setHgap(10);
			gp.setVgap(10);
			//Add vbMain to GridPane
			gp.add(vbMain, 0, 0);
			
			//Implement stylsheet
			bp.getStylesheets().add("hilo.css");
			
			/**Add component to borderpane layout*/
			//Add Menu Bar to BorderPane and gridpane
			bp.setTop(mBar);
			bp.setCenter(gp);
			
			//Create scene
			Scene s = new Scene(bp);
			
			//Set scene
			pStage.setScene(s);
			//Show scene
			pStage.show();
		}
		
		/***********************About dialog*************************/
		public void showAbout() 
		{
			//Create new stage
			Stage dStage = new Stage();
			//Set title
			dStage.setTitle("About");
			
			//Dialogstage is not resizeable
			dStage.setResizable(false);
			
			//Dialog stage should have focus text
			dStage.setAlwaysOnTop(true);
			
			//Create layouts
			VBox vbAbout = new VBox();
			BorderPane bpLabel = new BorderPane();
			
			//Add spacing and padding to vbAbout
			vbAbout.setSpacing(20);
			vbAbout.setPadding(new Insets(20));
			
			//Create label
			Label lblAuthor = new Label("Joseph Dappa\n2957899");
			
			//Set lblAuthor to centre of bpLabel
			bpLabel.setCenter(lblAuthor);
			//Append bpLabel to vbAbout
			vbAbout.getChildren().add(bpLabel);
			
			//Create scene and parse the size of vbAbout
			Scene s = new Scene(vbAbout, 200, 100);
			//Set scene
			dStage.setScene(s);
			//Show scene
			dStage.show();	
		}//End of showAbout()

		/******************Method to retrieve the image**************************/
		public Image retieveImg()
		{
			//Create a file instant
			File cardFile = new File("cards/"+deck.dealTopCard());
			return img = new Image(cardFile.toURI().toString());
		}// End of retieveImg()
		
		
		/*****************Deal First Card Method****************************/
		public void dealFirstCard()
		{	
			//This "if statement" make sure every new game is shuffled before playing
			if(gameplayed==0 && shuffled==0)
				showMessage("#9E260C", "Shuffle before playing.","top");
			/*Check if the first card had just been dealt.
			*  This is used to discourage multiple dealing of first card*/
			else if(btnCheck==1)
				showMessage("#9E260C", "You can only deal first card once","top");
			else//else deal a card
			{
				//Display the retrieve image on the image view
				imgViewFirstCard.imageProperty().setValue(retieveImg());
				//Disable both the radio buttons and the deal second card button
				btnDisable(false);
				//Increment to show the number of game played
				++gameplayed;
				//button toggle. After first card was dealt, the btnCheck becomes 1
				++btnCheck;
				//An indicator at the top of the UI to show the number of rounds played 
				showMessage("black", "Round: "+gameplayed,"top");
				lblResult.setText("");//Clear the result label	
				//Clear the second card of the previous	round
				imgViewSecondCard.imageProperty().setValue(null);
			}
		}
		
		
		/***************Deal Second Card Method*********************************/
		public void dealSecondCard()
		{	/*Check if the second card had just been dealt.
			* This is used to discourage multiple dealing of first card*/
			if(btnCheck==0)
				showMessage("#9E260C", "Deal first card","top");
			/*This make sure the any of the radio buttons is
			 * selected before 2nd card can be dealt*/
			else if(tg.getSelectedToggle()==null)
				showMessage("#9E260C", "Select High or Low","top");
			else
			{
				//Display the retrieve image on the image view
				imgViewSecondCard.imageProperty().setValue(retieveImg());
				//An indicator at the top of the UI to show the number of rounds played 
				showMessage("black", "Round: "+gameplayed,"top");
				//Calling radioBtnSelector() method
				radioBtnSelector();
				//Calling game over method to check if game is over
				gameOver();
				//set the toggle to null
				tg.selectToggle(null);
				
				// disable radio buttons after 2nd card was dealt
				rbHigher.setDisable(true);
				rbLower.setDisable(true);
				//button toggle. After second card was dealt, the btcnCheck becomes 0
				btnCheck--;	
			}
		}//End of deal second card method
		
		
		/************RadioButton selector method********************/
		public void radioBtnSelector()
		{
			//When the radio button higher is selected
			if(rbHigher.isSelected())
			{	
				/*Check if the second deal is greater than the first deal else you lose*/
				if(deck.checkGreater())
				{
					showMessage("green","You win","bottom");
					progressBarInd(++numOfWins);
					lblWinScore.setText("Score: "+numOfWins+"/5");
				}
				else
				{
					showMessage("#9E260C","You lose","bottom");
					numOfWins=0;
					progressBarInd(numOfWins);
					lblWinScore.setText("Score: "+numOfWins+"/5");
				}
			}//End of higher radio button
			//When the radio button lower is selected
			if(rbLower.isSelected())
			{
				/*Check if the second deal is lesser than the first deal else you lose*/
				if(deck.checkLower())
				{
					showMessage("green","You win","bottom");
					progressBarInd(++numOfWins);
					lblWinScore.setText("Score: "+numOfWins+"/5");
				}
				else
				{
					showMessage("#9E260C","You lose","bottom");
					numOfWins=0;
					progressBarInd(numOfWins);
					lblWinScore.setText("Score: "+numOfWins+"/5");
				}
			}//End of lower radio button
		}//End of radioBtnSelector()
		
		
		/****************Game Over Method***************/
		public void gameOver()
		{
			//Set to high when game is over
			endGame = true;
			//When you have five consecutive wins
			if(numOfWins==consecutiveWins)
			{
				allBtnsDisable();//disable all buttons
				showMessage("green", "\tCongrats! you won", "top");
				bouncingAlert();//The message should bounce
			}
			//When all the cards are played without five consecutive wins
			if(gameplayed==maxGame)
			{
				allBtnsDisable();//disable all buttons
				showMessage("#9E260C", "\tYou lose the game.", "top");
				bouncingAlert();//The message should bounce
			}
			lblResult.visibleProperty().setValue(null);//Make the result label invisible
		 }
		

		/*********Method to disable and enable both radio buttons and the second deal button*************/
		public void btnDisable(boolean state)
		{
			if(state==true)
			{
				rbHigher.setDisable(true);
				rbLower.setDisable(true);
				btnSecondDeal.setDisable(true);
			}
			else
			{
				rbHigher.setDisable(false);
				rbLower.setDisable(false);
				btnSecondDeal.setDisable(false);
			}
		}
		
		
		/***************Disable all the buttons on the application********/
		public void allBtnsDisable()
		{
			btnFirstDeal.setDisable(true);
			btnSecondDeal.setDisable(true);
			rbHigher.setDisable(false);
			rbLower.setDisable(false);
		}
		
		
		/************New Game Method**************/
		/* In the new game method, every function
		 * will be reset or set to null
		 * */
		public void newGame()
		{
			btnDisable(true);
			btnFirstDeal.setDisable(false);
			imgViewFirstCard.imageProperty().setValue(null);
			imgViewSecondCard.imageProperty().setValue(null);
			showMessage("black","New Game", "top");
			showMessage("black","", "bottom");
			tg.selectToggle(null);
			gameplayed=0;
			numOfWins=0;
			btnCheck=0;
			shuffled=0;
			endGame=false;
			lblWinScore.setText("Win: "+numOfWins+"/5");
			progressBarInd(0);
			deck = new DeckOfCards();
			deck.restart();
		}
		
		/********Method for Progress bar and Progress indicator*********/
		public void progressBarInd(int in)
		{
			// Get the current progress
			double progressB = progBar.getProgress();
			double progressI = progInd.getProgress();
			//Check if input in is greater than 0
			if(in>0)
			{
				// Increase the progress bar and indicator
				progressB =progressB + 0.2;
				progressI =progressI + 0.2;
				
				//Change the colour
				progBar.setStyle("-fx-accent:#FA5882");
				progInd.setStyle("-fx-accent:#FA5882");
				
				//Force the progressBar to one when it is greater than one
				if (progressB > 1)
					progressB = 1;
				
				//Set the progress
				progBar.setProgress(progressB);
				progInd.setProgress(progressI);
			}
			else
			{
				//Set the progress
				progBar.setProgress(in);
				progInd.setProgress(in);
			}
		}
		
		/******Method to display output on the GUI********/
		/*
		 * Since there are many outputs, this method is called anywhere 
		 * an output will be display.*/
		public void showMessage(String colour, String message, String pos)
		{
			//If the position is "top", then display output on the top of the GUI.
			if(pos.equalsIgnoreCase("top"))
			{
				lblTopMessage.setTextFill(Color.web(colour));
				lblTopMessage.setText(message);
			}
			//If the position is "bottom", then display output on the bottom of the GUI.
			if(pos.equalsIgnoreCase("bottom"))
			{
				lblResult.setTextFill(Color.web(colour));
				lblResult.setText(message);
			}
		}
		
		/**************Animation********************/
		/*This method makes the top label to bounce when you win or lose the game*/
		 public void bouncingAlert()
		 {
			 //A timeline object is created
			 final Timeline timeline = new Timeline();
			 //This object cycle count is set to six
			 timeline.setCycleCount(6);
			 //It is set to auto reverse meaning it bounces.
			 timeline.setAutoReverse(true);
			 /*The timeline object duration to bounce, the direction of the 
			  *bounce and the component that will bounce are created*/
			 timeline.getKeyFrames().add(new KeyFrame(Duration.millis(500),new KeyValue (lblTopMessage.translateYProperty(), 50)));
			 //The timeline object =>"lblTopMessage" is made to bounce.
			 timeline.play();
		 }
		 
		 
		/**************The Main Method*******************/
		public static void main(String[] args) 
		{
			//Launch the application
			launch();
		}//End of main method

}//End of HiloGame.java